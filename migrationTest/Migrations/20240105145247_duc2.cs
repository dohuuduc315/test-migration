﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace migrationTest.Migrations
{
    /// <inheritdoc />
    public partial class duc2 : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_Chas",
                table: "Chas");

            migrationBuilder.DropColumn(
                name: "Con_Ten",
                table: "Chas");

            migrationBuilder.DropColumn(
                name: "Con_Tuoi",
                table: "Chas");

            migrationBuilder.DropColumn(
                name: "Diachi",
                table: "Chas");

            migrationBuilder.DropColumn(
                name: "Discriminator",
                table: "Chas");

            migrationBuilder.DropColumn(
                name: "Ten",
                table: "Chas");

            migrationBuilder.DropColumn(
                name: "Tuoi",
                table: "Chas");

            migrationBuilder.RenameTable(
                name: "Chas",
                newName: "Cha");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Cha",
                table: "Cha",
                column: "Id");

            migrationBuilder.CreateTable(
                name: "Chau",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false),
                    Ten = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Tuoi = table.Column<int>(type: "int", nullable: false),
                    Diachi = table.Column<string>(type: "nvarchar(max)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Chau", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Chau_Cha_Id",
                        column: x => x.Id,
                        principalTable: "Cha",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Con",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false),
                    Ten = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Tuoi = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Con", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Con_Cha_Id",
                        column: x => x.Id,
                        principalTable: "Cha",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Chau");

            migrationBuilder.DropTable(
                name: "Con");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Cha",
                table: "Cha");

            migrationBuilder.RenameTable(
                name: "Cha",
                newName: "Chas");

            migrationBuilder.AddColumn<string>(
                name: "Con_Ten",
                table: "Chas",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "Con_Tuoi",
                table: "Chas",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Diachi",
                table: "Chas",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Discriminator",
                table: "Chas",
                type: "nvarchar(5)",
                maxLength: 5,
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "Ten",
                table: "Chas",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "Tuoi",
                table: "Chas",
                type: "int",
                nullable: true);

            migrationBuilder.AddPrimaryKey(
                name: "PK_Chas",
                table: "Chas",
                column: "Id");
        }
    }
}
