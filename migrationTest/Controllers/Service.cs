﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System.Reflection;

namespace migrationTest.Controllers
{


    public class Cha
    {
        public int Id { get; set; }
        public string UserName { get; set; }
        public string Pass { get; set; }
    }


    public class Con : Cha
    {
        public string Ten { get; set; }
        public int Tuoi { get; set; }
    }

    public class Chau : Cha
    {
        public string Ten { get; set; }
        public int Tuoi { get; set; }
        public string Diachi { get; set; }
    }

    public class AppDbContext : DbContext
    {
        public AppDbContext(DbContextOptions<AppDbContext> options) : base(options)
        {
        }

        public DbSet<Con> Cons { get; set; }
        public DbSet<Cha> Chas { get; set; }
        public DbSet<Chau> Chaus { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfigurationsFromAssembly(Assembly.GetExecutingAssembly());
            base.OnModelCreating(modelBuilder);
        }
    }

    public class ChaConfig : IEntityTypeConfiguration<Cha>
    {
        public void Configure(EntityTypeBuilder<Cha> builder)
        {
            builder.ToTable(nameof(Cha));
        }
    }

    public class ConConfig : IEntityTypeConfiguration<Con>
    {
        public void Configure(EntityTypeBuilder<Con> builder)
        {
            builder.ToTable(nameof(Con)).HasBaseType<Cha>();
        }
    }

    public class ChauConfig : IEntityTypeConfiguration<Chau>
    {
        public void Configure(EntityTypeBuilder<Chau> builder)
        {
            builder.ToTable(nameof(Chau)).HasBaseType<Cha>();
        }
    }



}