﻿using migrationTest.Controllers;

namespace migrationTest
{
    public interface IConService {


        Task Create();
    }

    public class ConService : IConService
    {
        private readonly AppDbContext _context;

        public ConService(AppDbContext context)
        {
            _context = context;
        }

        public async Task Create()
        {
            var con = new Con
            {
                UserName = "Con",
                Pass = "123",
                Ten = "Con",
                Tuoi = 12
            };

            Cha con2 = new Con
            {
                UserName = "Con2",
                Pass = "123",
                Ten = "Con2",
                Tuoi = 15
            };

            await _context.AddAsync(con);
            await _context.AddAsync(con2);
            await _context.SaveChangesAsync();
        }
    }
}
