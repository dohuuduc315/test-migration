﻿// See https://aka.ms/new-console-template for more information
Console.WriteLine("Hello, World!");

Father kid = new Kid
{
    FullName = string.Empty,
    Id = 2,
    UserName = string.Empty,
    Pass = string.Empty,
};

Console.WriteLine(kid is Father);

class Father
{
    public int Id { get; set; }
    public string UserName { get; set; }
    public string Pass { get; set; }
}

class Kid : Father
{
    public string FullName { get; set; }
}

class GrandSon: Father
{
    public int Age { get; set; }
}